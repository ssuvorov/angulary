angular.module('module-ui')
    .directive('uiCombobox', [ function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
            	value: '=ngModel',
            	disabled: '=disabled'
            },
            controller: ["$scope", function($scope) {
            	//
            	$scope.open	= false;
            	//
            	$scope._protected	= {
            		selectByIndex: function(index) {
            			console.log("selectByIndex: " + index);
            		},
            		selectByValue: function(value) {
            			console.log("selectByValue: " + value);
            		}
            	};
            }],
            template: 
            	'<div class="ui-combobox">\
            		<input class="ui-combobox--input" disabled="{{disabled}}" />\
            		<div style="position:relative; border-color: inherit; border-style: inherit; border-width: 0px;" onmousedown="event.preventDefault()">\
            			<div ng-transclude="true" class="ui-combobox--dropdown" style="display:none"></div>\
            		</div>\
            	</div>',
            link: function($scope, element) {
            	//
            	angular.element(element.children()[0]).bind('focus', function(event) {
            		angular.element(element.children()[1]).children()[0].style.display	= '';
            	});
            	angular.element(element.children()[0]).bind('blur', function(event) {
            		angular.element(element.children()[1]).children()[0].style.display	= 'none';
            	});
            	
            	// select item
            	//$scope._protected.selectByValue($scope.value);
            }
        }
    }]);