angular.module('module-ui')
    .directive('uiContainer', [ function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            template: '<div class="ui-container" ng-transclude></div>'
        }
    }]);