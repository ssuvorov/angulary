angular.module('module-ui')
    .directive('uiComboitem', [ function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
            	value: "@value"
            },
            template: '<div class="ui-comboitem" ng-transclude></div>',
        	link: function($scope, element) {
            	element.bind('click', function(event) {
            		$scope.$parent._protected.selectByValue(element.attr("value"));
            	});
            }
        }
    }]);