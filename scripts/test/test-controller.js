angular.module('module-app')
	.controller("testController", ["$scope", function($scope) {
		$scope.options	= [
			["en", "English"],
			["ru",	"Russian"],
			["nl",	"Dutch"]
		];
		
		$scope.first	= "en";
		$scope.second	= "ru";
		$scope.third;
		
		//
		$scope.includeDutch	= false;
		
		$scope.change = function(){
			$scope.includeDutch = !$scope.includeDutch;
		}
		
		//
		$scope.$watch("includeDutch", function(newValue) {
			console.log(newValue);
			if (newValue) {
				if ($scope.options.length < 3)
					$scope.options.push(["nl",	"Dutch"]);
			}
			else {
				$scope.options.length	= 2;
			}

		});
		
		
	}]);